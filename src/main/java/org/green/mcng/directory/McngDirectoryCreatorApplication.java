package org.green.mcng.directory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class McngDirectoryCreatorApplication {
	public static void main(String[] args) {
		SpringApplication.run(McngDirectoryCreatorApplication.class, args);
	}
}
