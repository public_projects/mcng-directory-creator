package org.green.mcng.directory.config;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.green.mcng.directory.config.model.VmConfigProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Map;


@Configuration
@ConfigurationProperties(prefix = "mcng")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class McngDirectoryCreatorConfiguration {
    Map<String, VmConfigProperties> env;
}
