package org.green.mcng.directory.config.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class VmConfigProperties {
    private String ip;
    private String pathPrefix;
    private int newRecordCount;
    private int prefixLength;
    private String username;
    private String password;
    private String ipfMcngConsumerPath;
    private String mcngDataFilesDirectory;
    private int intervalBetweenFiles;
    private boolean skipMove;
    private String handlerName;
}
