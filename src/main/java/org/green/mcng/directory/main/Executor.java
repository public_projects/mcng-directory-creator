package org.green.mcng.directory.main;

import lombok.extern.slf4j.Slf4j;
import org.green.mcng.directory.config.McngDirectoryCreatorConfiguration;
import org.green.mcng.directory.config.model.VmConfigProperties;
import org.green.mcng.directory.handler.LocalMoveHandler;
import org.green.mcng.directory.handler.RemoteUploadHandler;
import org.green.mcng.directory.util.McngFileMover;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PreDestroy;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class Executor implements CommandLineRunner, ApplicationContextAware {
    public static final String SKIP_MOVE = "skipMove";
    public static final String ENV = "env";
    private ApplicationContext context;

    @Autowired
    private McngFileMover mcngFileMover;

    @Autowired
    private McngDirectoryCreatorConfiguration mcngDirectoryCreatorConfiguration;

    private boolean createDirectory(String directoryToCreate) {
        try {
            Path path = Paths.get(directoryToCreate);

            //java.nio.file.Files;
            Files.createDirectories(path);
            log.info("Successfully created processing directory: {}", directoryToCreate);
            return true;
        }
        catch (Exception e) {
            log.error("Failed to create directory: {}", directoryToCreate, e);
        }
        return false;
    }

    @Override
    public void run(String... args) {
//        args = new String[1];
//        args[0] = "env=DITA3";
        VmConfigProperties executionArguments = extractUserArguments(args);

        if (!createDirectory(executionArguments.getMcngDataFilesDirectory())) {
            ((ConfigurableApplicationContext) context).close();
            log.info("Shutdown! Bye Bye!");
            return;
        }

        try {
            if (executionArguments.getHandlerName()
                                  .equalsIgnoreCase("LocalMoveHandler")) {
                LocalMoveHandler localMoveHandler = new LocalMoveHandler();
                localMoveHandler.moveFiles(executionArguments);
            }
            else {
                RemoteUploadHandler remoteUploadHandler = new RemoteUploadHandler(executionArguments);
                remoteUploadHandler.createMoveRemoveDirectory(executionArguments.getMcngDataFilesDirectory(),
                                                              executionArguments.getIpfMcngConsumerPath(),
                                                              executionArguments.getPathPrefix(),
                                                              executionArguments.getIntervalBetweenFiles());
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }

        ((ConfigurableApplicationContext) context).close();
        log.info("DONE. shutdown! Bye Bye!");
        return;
    }

    private VmConfigProperties extractUserArguments(String[] args) {
        Map<String, Object> arguments = new HashMap<>();
        arguments.put(SKIP_MOVE, new Boolean(false));

        if (args != null) {
            for (String arg : args) {
                System.err.println("given argument: "+arg);
                if (arg.contains(ENV)) {
                    String[] property = StringUtils.split(arg, "=");
                    System.err.println("env is found!" + property[1]);
                    arguments.put(ENV, property[1]);
                }

                if (arg.equals(SKIP_MOVE)) {
                    arguments.put(SKIP_MOVE, new Boolean(true));
                }
            }
        }

        if (arguments.isEmpty()) {
            System.err.println("Missing argument [\"env\", \"skipMove\"]!");
        }

        Object environment = arguments.get(ENV);

        if (environment == null) {
            log.info("Given environment is null", environment);
            System.exit(1);
        }

        System.err.println("env object: "+ environment);

        VmConfigProperties vmConfigProperties = mcngDirectoryCreatorConfiguration.getEnv()
                                                                                 .get(environment);

        if (vmConfigProperties == null) {
            log.info("Given environment not fount in application.yml: {}", environment);
            System.exit(1);
        }

        vmConfigProperties.setSkipMove((Boolean) arguments.get(SKIP_MOVE));

        return vmConfigProperties;
    }

    @PreDestroy
    public void onShutDown() {
        System.out.println("closing application context..let's do the final resource cleanup");
    }

    @Override
    public void setApplicationContext(ApplicationContext ctx) throws BeansException {
        this.context = ctx;
    }
}
