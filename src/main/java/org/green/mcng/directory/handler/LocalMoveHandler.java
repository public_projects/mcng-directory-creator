package org.green.mcng.directory.handler;

import lombok.extern.slf4j.Slf4j;
import org.green.mcng.directory.util.McngFileMover;
import org.green.mcng.directory.config.model.VmConfigProperties;
import org.springframework.stereotype.Service;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.stream.Stream;

@Service
@Slf4j
public class LocalMoveHandler {

    public void moveFiles(VmConfigProperties vmConfigProperties) throws Exception {
        Path latestPath = null;
        try (Stream<Path> paths = Files.walk(Paths.get(vmConfigProperties.getIpfMcngConsumerPath()))) {
            latestPath = paths.filter(Files::isDirectory)
                              .filter(path -> path.getFileName()
                                                  .toString()
                                                  .contains(vmConfigProperties.getPathPrefix()))
                              .sorted(Comparator.reverseOrder())
                              .findFirst()
                              .get();

            log.info("[path.toString()] {} ", latestPath.toString());
        }
        createNewDirectory(vmConfigProperties);
    }

    private boolean isCurrentDate(LocalDate latestDate) {
        LocalDate currentDate = LocalDate.now();
        return !currentDate.isAfter(latestDate);
    }

    private void createNewDirectory(VmConfigProperties vmConfigProperties) throws Exception {
        if (vmConfigProperties.isSkipMove()) {
            McngFileMover.createMoveLocalDirectory(vmConfigProperties.getMcngDataFilesDirectory(),
                                     vmConfigProperties.getMcngDataFilesDirectory(),
                                     vmConfigProperties.getPathPrefix(), 1);
        } else {
            McngFileMover.createMoveLocalDirectory(vmConfigProperties.getMcngDataFilesDirectory(),
                                     vmConfigProperties.getIpfMcngConsumerPath(),
                                     vmConfigProperties.getPathPrefix(),
                                     vmConfigProperties.getIntervalBetweenFiles());
        }
    }


}
