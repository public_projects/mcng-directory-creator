package org.green.mcng.directory.handler;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import lombok.extern.slf4j.Slf4j;
import org.green.mcng.directory.config.model.VmConfigProperties;
import org.green.mcng.directory.util.McngFileMover;

import java.io.File;
import java.io.FileInputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;


@Slf4j
public class RemoteUploadHandler {
    private ChannelSftp channelSftp;

    public RemoteUploadHandler(VmConfigProperties vmConfigProperties) {
        this.channelSftp = getChannel(vmConfigProperties.getIp(),
                                      vmConfigProperties.getUsername(),
                                      vmConfigProperties.getPassword());
    }

    public void createMoveRemoveDirectory(String inputDirectory, String outputDirectory, String prefix, long sleepTime)
            throws Exception {

        log.info("New output directory is: {}", outputDirectory);

        List<File> availableFiles = McngFileMover.getAvailableFiles(inputDirectory);
        String formattedDateStr = null;
        String formattedOutputFolderName = "";

        channelSftp.connect();
        for (int i = 0; i < availableFiles.size(); i++) {
            formattedDateStr = LocalDateTime.now()
                                            .format(DateTimeFormatter.ofPattern("yyyyMMddHmsSSS"));
            formattedOutputFolderName = prefix + formattedDateStr;

            File newFile = McngFileMover.createMoveLocalFile(inputDirectory, formattedOutputFolderName, availableFiles.get(i), 10l);


            log.info("Newly created directory: {}",
                     newFile.getParentFile()
                            .getAbsolutePath());


            copy(newFile.getParentFile(), outputDirectory, sleepTime);
        }
        channelSftp.disconnect();
    }

    public ChannelSftp getChannel(String hostIp, String username, String password) {
        try {
            JSch jsch = new JSch();
            jsch.setKnownHosts("C:\\Users\\sm13\\.ssh\\known_hosts");
            Session jschSession = jsch.getSession(username, hostIp);
            jschSession.setPassword(password);
            jschSession.connect();
            return (ChannelSftp) jschSession.openChannel("sftp");
        }
        catch (Exception exception) {
            exception.printStackTrace();
            System.exit(1);
        }
        return null;
    }

    public void copy(File localFile, String destPath, long sleepTime) {
        try {
            log.info("Copying directory: {}, to: {}", localFile.getAbsolutePath(), destPath);
            channelSftp.cd(destPath);
            if (localFile.isDirectory()) {
                channelSftp.mkdir(localFile.getName());
                destPath = destPath + "/" + localFile.getName();
                channelSftp.cd(destPath);

                if (localFile.listFiles().length > 1) {
                    log.info("Multiple files in: " + localFile.getAbsolutePath() + System.lineSeparator());
                }

                if (localFile.listFiles().length == 0) {
                    log.info("There was no files in: " + localFile.getAbsolutePath() + System.lineSeparator());
                }

                for (File file : localFile.listFiles()) {
                    copy(file, destPath, sleepTime);
                }
                channelSftp.cd(destPath.substring(0, destPath.lastIndexOf('/')));
            }
            else {
                channelSftp.put(new FileInputStream(localFile), localFile.getName(), ChannelSftp.OVERWRITE);
            }

            Thread.sleep(sleepTime);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            channelSftp.disconnect();
        }
    }
}
