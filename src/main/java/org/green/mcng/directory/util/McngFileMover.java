package org.green.mcng.directory.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class McngFileMover {
    public static final List<String> MCNG_FILES;

    static {
        MCNG_FILES = new ArrayList<>();
        MCNG_FILES.add("P_MCCASE_FLAT.csv");
        MCNG_FILES.add("P_TARGET_FLAT.csv");
        MCNG_FILES.add("P_MCCASE_TARGET_FLAT.csv");
        MCNG_FILES.add("P_TARGET_MSISDN_FLAT.csv");
        MCNG_FILES.add("P_TARGET_IMSI_FLAT.csv");
        MCNG_FILES.add("P_TARGET_IMEI_FLAT.csv");
        MCNG_FILES.add("P_TARGET_LIID_FLAT.csv");
        MCNG_FILES.add("P_CALL_FLAT.csv");
        MCNG_FILES.add("P_SMS_FLAT.csv");
        MCNG_FILES.add("P_LOC_FLAT.csv");
        MCNG_FILES.add("P_CONTENTLOC_FLAT.csv");
        MCNG_FILES.add("P_CELL_FLAT.csv");
    }

    public static List<File> getAvailableFiles(String inputDirectory) {
        List<File> filesToMove = new ArrayList<>();

        // Files follow order
        for (String fileName : MCNG_FILES) {
            File mcngInputDataFile = new File(inputDirectory + "\\" + fileName);

            if (mcngInputDataFile.exists()) {
                log.info("File exist: {}", mcngInputDataFile);
                filesToMove.add(mcngInputDataFile);
            }
        }
        return filesToMove;
    }

    public static void createMoveLocalDirectory(String inputDirectory,
                                                String outputDirectory,
                                                String prefix,
                                                long sleepTime)
            throws Exception {

        log.info("New output directory is: {}", outputDirectory);

        List<File> availableFiles = McngFileMover.getAvailableFiles(inputDirectory);
        String formattedDateStr = null;
        String formattedOutputFolderName = "";

        for (File availableFile : availableFiles) {
            formattedDateStr = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHmsSSS"));
            formattedOutputFolderName = prefix + formattedDateStr;

            File newFile = createMoveLocalFile(inputDirectory, formattedOutputFolderName, availableFile, 10l);

            prepareMoveFile(outputDirectory, formattedOutputFolderName, newFile, sleepTime);
        }
    }

    public static File createMoveLocalFile(String outputFileDirectory,
                                       String formattedOutputFolderName,
                                       File processingFile,
                                       Long sleepTime) throws Exception {
        File newDir = new File(outputFileDirectory + "\\" + formattedOutputFolderName);
        log.info("Created: {}", newDir.getAbsolutePath());
        newDir.mkdir();
        File newFile = new File(newDir.getAbsolutePath() + "\\" + processingFile.getName());

        if (!McngFileMover.moveFile(processingFile, newFile)) {
            log.info("Failed to move file {} to {}",
                     processingFile.getAbsolutePath(),
                     newFile.getAbsolutePath());
        }

        Thread.sleep(sleepTime);

        return newFile;
    }

    public static void prepareMoveFile(String outputFileDirectory,
                                       String formattedOutputFolderName,
                                       File processingFile,
                                       Long sleepTime) throws Exception {
        File newDir = new File(outputFileDirectory + "\\" + formattedOutputFolderName);
        log.info("Created: {}", newDir.getAbsolutePath());
        newDir.mkdir();
        File newFile = new File(newDir.getAbsolutePath() + "\\" + processingFile.getName());

        if (!McngFileMover.copyFile(processingFile, newFile)) {
            log.info("Failed to move file {} to {}",
                     processingFile.getAbsolutePath(),
                     newFile.getAbsolutePath());
        }

        Thread.sleep(sleepTime);
    }

    public static boolean moveFile(File from, File to) throws Exception {
        Path temp = Files.move(from.toPath(), to.toPath(), StandardCopyOption.ATOMIC_MOVE);

        if (temp != null) {
            return true;
        }
        return false;
    }

    public static boolean copyFile(File from, File to) throws Exception {
        Path temp = Files.copy(from.toPath(), to.toPath());

        if (temp != null) {
            return true;
        }
        return false;
    }
}
